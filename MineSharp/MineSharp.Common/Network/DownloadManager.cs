using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using Ionic.Zip;
using MineSharp.Common.Data;
using MineSharp.Common.Data.Internal;
using MineSharp.Common.Game;
using MineSharp.Common.Util;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MineSharp.Common.Network
{
    public static class DownloadManager
    {

        
        /// <summary>
        /// Downloads Versions Manifest containing all Minecraft Versions
        /// </summary>
        /// <returns>VersionManifest Object</returns>
        public static VersionsManifest DownloadVersionsManifest()
        {
            //Download version data
            var wcl = new WebClient();
            var dat = wcl.DownloadString("https://launchermeta.mojang.com/mc/game/version_manifest.json");
            return JsonConvert.DeserializeObject<VersionsManifest>(dat);
        }

        /// <summary>
        /// Downloads Manifest for specified Minecraft Version, extension for VersionsManifest
        /// </summary>
        /// <param name="manifest"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public static MinecraftVersionManifest DownloadVersion(this VersionsManifest manifest, string version)
        {
            //Download version data
            var v = manifest.GetVersionData(version);
            if (v == null) return null;
            
            //Download Version manifest
            var wcl = new WebClient();
            var dat = wcl.DownloadString(v.Url);
            dat = dat.Replace("natives-windows", "natives_windows").Replace("natives-osx", "natives_osx")
                .Replace("natives-linux", "natives_linux");
            
            //Parse version manifest
            return JsonConvert.DeserializeObject<MinecraftVersionManifest>(dat);
        }

        /// <summary>
        /// Creates AsyncDownloadOpertaion for downloading Libraries, extension for MinecraftVersionManifest
        /// </summary>
        /// <param name="manifest"></param>
        /// <param name="libPath">Libraries Directory</param>
        /// <param name="nativesPath">Natives Directory</param>
        /// <param name="isForgeManifest"></param>
        /// <param name="mavenPath"></param>
        /// <returns></returns>
        public static AsyncDownloadOperation DownloadLibraries(this MinecraftVersionManifest manifest, string libPath,
            string nativesPath, bool isForgeManifest = false, string mavenPath = "")
        {
            //Create new AsyncDownloadOperation
            var ado = new AsyncDownloadOperation();
            try
            {
                //Set total objects to download to be equal libraries count                
                ado.Total = manifest.Libraries.Count;
                
                //Create download Thread
                var t = new Thread(() =>
                {
                    try
                    {
                        //Fix Directories if does not end with slash
                        if (!libPath.EndsWith("/")) libPath += "/";
                        if (!nativesPath.EndsWith("/")) nativesPath += "/";

                        if (!Directory.Exists(libPath))
                            Directory.CreateDirectory(libPath);

                        if (!Directory.Exists(nativesPath))
                            Directory.CreateDirectory(nativesPath);

                        //Download all libraries
                        foreach (var l in manifest.Libraries)
                        {
                            //if (isForgeManifest) Console.WriteLine("FORGE!");
                            
                            //Asset Path
                            var fn = "";

                            //If should be downloaded then do so
                            if (!isForgeManifest)
                            {
                                if (!l.Downloads.IsNative)
                                    fn = libPath + l.Downloads.GetArtifact().Path;
                                else
                                    fn = nativesPath + "temp.jar";

                                //Console.WriteLine("Downloading: " + l.Name);
                                
                                //Gets Directory name and creates directories recursively
                                var fp = Path.GetDirectoryName(fn);
                                if (!Directory.Exists(fp))
                                    Directory.CreateDirectory(fp);

                                //Temp variable to define if file should be downloaded
                                var toDownload = false;
                                if (File.Exists(fn))
                                {
                                    toDownload = false;

                                    //Validate artifact checksum
                                    var sha = Metadata.GetSha1(fn);
                                    if (sha != l.Downloads.GetArtifact().Sha1)
                                    {
                                        toDownload = true;
                                    }
                                }
                                else
                                {
                                    toDownload = true;
                                }

                                if (toDownload)
                                { 
                                    var uri = l.Downloads.GetArtifact().Url;
                                    
                                    //Create WebClient and download File
                                    var wcl = new WebClient();
                                    var isNative = uri.Contains("natives");

                                    wcl.DownloadFile(uri.Replace("natives_", "natives-"), fn);

                                    //If library is native library extract all natives
                                    if (isNative)
                                    {
                                        using (var zf = ZipFile.Read(fn))
                                        {
                                            zf.ExtractAll(Path.GetDirectoryName(fn),
                                                ExtractExistingFileAction.OverwriteSilently);
                                        }
                                    }
                                }
                                
                                // Try download native
                                fn = nativesPath + "temp.jar";
                                var artifact = l.Downloads.TryGetNativesArtifact();
                                if (artifact != null)
                                {

                                    //Console.WriteLine("Natives found!");
                                    
                                    var uri = artifact.Url;
                                    var isNative = uri.Contains("natives");
                                
                                    //Create WebClient and download File
                                    var wcl = new WebClient();
                                    wcl.DownloadFile(uri.Replace("natives_", "natives-"), fn);
                                
                                    //If library is native library extract all natives
                                    if (isNative)
                                    {
                                        using (var zf = ZipFile.Read(fn))
                                        {
                                            zf.ExtractAll(Path.GetDirectoryName(fn),
                                                ExtractExistingFileAction.OverwriteSilently);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                //Set operation to be equal library name (for using async display of current download state)
                                ado.Operation = l.Name;

                                var uri = l.Url;
                                if (string.IsNullOrEmpty(uri))
                                    uri = "https://modloaders.forgecdn.net/647622546/maven2/";
                                
                                // Prevents server-only libs
                                if (!string.IsNullOrEmpty(uri))
                                {
                                   // var fName = l.Name.Replace(":", "").Replace("-", "").Replace(".", "");
                                   var fName = LauncherFrame.LibFileNameFix(l.Name);
                                    if (fName.Contains("wrapper"))
                                        uri = "http://itnnovative.com:8081/artifactory/libs-release-local/";
                                    if (fName.Contains("lzma"))
                                        uri = "https://repo.spongepowered.org/maven/";
                                    
                                    
                                    var psName =
                                        "dependency:get -DrepoUrl=\"" + uri + "\" -Dartifact=\"" +
                                        l.Name + "\" -Dtransitive=false -Ddest=\"" + fName + "\"";

                                    //Console.WriteLine(psName);
                                    var fNames = fName.Split('/').ToList();
                                    fNames.RemoveAt(fNames.Count - 1);
                                    var newName = String.Join("/", fNames.ToArray());
                                    
                                    if (!Directory.Exists(libPath + "/" + newName))
                                    {
                                        Directory.CreateDirectory(newName);
                                    }
                                    
                                    if (!File.Exists(libPath + "/" + fName + ".jar"))
                                    {
                                        // Maven downloads
                                      
                                        
                                        try
                                        {
                                            var startInfo = new ProcessStartInfo
                                            {
                                                WorkingDirectory = mavenPath,
                                                WindowStyle = ProcessWindowStyle.Hidden,
                                                FileName = mavenPath + "/mvn",
                                                RedirectStandardInput = false,
                                                UseShellExecute = true,
                                                Arguments = psName
                                            };

                                            var ps = Process.Start(startInfo);
                                            ps.WaitForExit();
                                            File.Move(mavenPath + "/" + fName, libPath + "/" + fName + ".jar");

                                        }
                                        catch (Exception ex)
                                        {
                                            //Console.WriteLine(ex.Message);
                                            //Console.WriteLine(ex.StackTrace);
                                        }
                                    }
                                }
                            }
                            
                            //Library has been downloaded, going into next
                            ado.Current++;
                            
                            //Add small sleep to prevent memory overflows
                            Thread.Sleep(16);
                        }
                    }
                    catch
                    {
                        //Something failed, but it should never happen unless the internet fails
                    }

                    //Skip all libraries
                    ado.Current = ado.Total;
                    
                });
                
                //Start Download
                t.Start();
            }
            catch
            {
                ado.Current = ado.Total;
            }

        
            return ado;
        }

        /// <summary>
        /// Downloads Minecraft
        /// </summary>
        /// <param name="manifest"></param>
        /// <param name="gameDir">Directory to download the game into</param>
        public static void DownloadMinecraft(this MinecraftVersionManifest manifest, string gameDir)
        {
            
            //Fix paths to contain slash at end
            if (!gameDir.EndsWith("/")) gameDir += "/";
            gameDir += "versions/";
            if (!Directory.Exists(gameDir + manifest.Id)) Directory.CreateDirectory(gameDir + manifest.Id);
            gameDir += manifest.Id + "/";
            
            //Create manifest in versions folder
            File.WriteAllText(gameDir + manifest.Id + ".json", JsonConvert.SerializeObject(manifest));
            var toDownload = false;

            //Check if game jar file exists and is equal to one in manifest
            if (File.Exists(gameDir + manifest.Id + ".jar"))
            {
                if (Metadata.GetSha1(gameDir + manifest.Id + ".jar") != manifest.Downloads.Client.Sha1)
                {
                    toDownload = true;
                }
            }
            else
            {
                toDownload = true;
            }

            //If does not exist or is wrong download
            if (toDownload)
            {
                var wcl = new WebClient();
                wcl.DownloadFile(manifest.Downloads.Client.Url, gameDir + manifest.Id + ".jar");
            }
            
            
        }

  
        /// <summary>
        /// Creates AsyncDownloadOperation for downloading Game Assets
        /// </summary>
        /// <param name="manifest"></param>
        /// <param name="assetsPath">Path to download Assets into</param>
        /// <returns>AsyncDownloadOperation</returns>
        public static AsyncDownloadOperation DownloadAssets(this MinecraftVersionManifest manifest, string assetsPath)
        {
            //Create AsyncDownloadOperation and set Total to assets count
            var ado = new AsyncDownloadOperation();
            
            //Get assets information
            var ai = manifest.GetAssetInformation();
            ado.Total = ai.Assets.Count;
            
            //Create Asset Index Required by Game -,-
            if (!Directory.Exists(assetsPath + "/indexes/"))
                Directory.CreateDirectory(assetsPath + "/indexes/");
            
            //Download assets manifest
            var wql = new WebClient();
            var aif = wql.DownloadString(manifest.AssetIndex.Url);
            File.WriteAllText(assetsPath + "/indexes/" + manifest.Assets +".json", aif);
            
            //Create Assets Download Thread
            var t = new Thread(() =>
            {
                //Fix path to contain slash at end
                if (!assetsPath.EndsWith("/")) assetsPath += "/";
                
                //If it's old version use legacy, otherwise objects
                assetsPath += ai.Virtual ? "virtual/legacy/" : "objects/";
                
                //Download all assets
                foreach (var a in ai.Assets)
                {
                    //Asset Path
                    var fn = "";
                    
                    //If asset is virtual then use title, directory and hash otherwise
                    if (ai.Virtual)
                        fn = assetsPath + a.Title;
                    else fn = assetsPath + a.GetFolder() + "/" + a.Hash;

                    //Create directories recursively if does not exist
                    var fp = Path.GetDirectoryName(fn);
                    if (!Directory.Exists(fp))
                        Directory.CreateDirectory(fp);

                    var toDownload = false;
                    
                    //Check if file has been already downloaded
                    if (File.Exists(fn))
                    {
                        //Verify checksum
                        var sha1 = Metadata.GetSha1(fn);
                        if (sha1 != a.Hash)
                        {
                            toDownload = true;
                        }
                    }
                    else
                    {
                        toDownload = true;
                    }
                    
                    //File has not been downloaded / is wrong
                    if (toDownload)
                    {
                        //Download the file and set ADO Title to be used as common display for launcher UI
                        ado.Operation = a.Title;
                        var wcl = new WebClient();
                        wcl.DownloadFile("http://resources.download.minecraft.net/" + a.GetFolder() + "/" + a.Hash, fn);
                    }
                    
                    //Asset downloaded
                    ado.Current++; 
                }

            });
            t.Start();
    
            return ado;
        }
        
        /// <summary>
        /// Get Assets Information
        /// </summary>
        /// <param name="manifest"></param>
        /// <returns>AssetsInformationList object</returns>
        public static AssetInformationList GetAssetInformation(this MinecraftVersionManifest manifest)
        {
            var data = new AssetInformationList();
            
            //Download assets manifest
            var wcl = new WebClient();
            var ai = wcl.DownloadString(manifest.AssetIndex.Url);
            
            //Parse assets manifest
            var jt = (JObject)JsonConvert.DeserializeObject(ai);
            var ch = jt.Children();
            foreach (var c in ch)
            {
                var jo = (JProperty) c;
                if (jo.Name == "objects")
                {
                    //Get Objects JSON Object
                    var jtl = jo.Value;
                    foreach (var v in jtl.Children())
                    {
                        //Get "objects" children to receive all assets data
                        var prop = (JProperty) v;
                        var a = prop.Value.ToObject<AssetInformation>();
                        a.Title = prop.Name;
                        data.Assets.Add(a);
                    }
                }
                else if (jo.Name == "virtual")
                {
                    data.Virtual = jo.Value.ToObject<bool>();
                }


            }

            
            
            return data;
        }
    }
}