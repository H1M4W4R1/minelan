namespace MineSharp.Common.Data.Internal
{
    /// <summary>
    /// Contains information about native libraries
    /// </summary>
    public class LibraryClassifierInformation
    {
        /// <summary>
        /// Windows natives
        /// </summary>
        public LibraryArtifactInformation Natives_windows;
        
        /// <summary>
        /// Linux natives
        /// </summary>
        public LibraryArtifactInformation Natives_linux;
        
        
        /// <summary>
        /// OSX natives
        /// </summary>
        public LibraryArtifactInformation Natives_osx;
    }
}