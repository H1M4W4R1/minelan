namespace MineSharp.Common.Data.Internal
{
    /// <summary>
    /// Contains information about game logging
    /// </summary>
    public class LoggingInformation
    {
        /// <summary>
        /// Logging for client
        /// </summary>
        public LoggingClientInformation Client;
    }
}