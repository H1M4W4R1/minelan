using System.Collections.Generic;

namespace MineSharp.Common.Data.Internal
{
    /// <summary>
    /// Represents set of Game Assets
    /// </summary>
    public class AssetInformationList
    {
        /// <summary>
        /// True if assets are virtual (old Minecraft Versions)
        /// </summary>
        public bool Virtual;
        
        /// <summary>
        /// List of all available assets
        /// </summary>
        public List<AssetInformation> Assets = new List<AssetInformation>();
    }
}