namespace MineSharp.Common.Data.Internal
{
    /// <summary>
    /// Represents download information of artifact
    /// </summary>
    public class DownloadsInformation
    {
        /// <summary>
        /// Download checksum
        /// </summary>
        public string Sha1;
        
        /// <summary>
        /// Download size
        /// </summary>
        public long Size;
        
        //Download URI
        public string Url;
    }
}