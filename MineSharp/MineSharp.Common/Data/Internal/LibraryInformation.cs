namespace MineSharp.Common.Data.Internal
{
    /// <summary>
    /// Contains information about library
    /// </summary>
    public class LibraryInformation
    {
        /// <summary>
        /// Library download possibilities
        /// </summary>
        public LibraryDownloadsInformation Downloads;
        
        /// <summary>
        /// Library name
        /// </summary>
        public string Name;

        /// <summary>
        /// Maven URL for Forge
        /// </summary>
        public string Url;
    }
}