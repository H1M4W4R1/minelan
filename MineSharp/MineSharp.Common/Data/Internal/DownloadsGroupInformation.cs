namespace MineSharp.Common.Data.Internal
{
    /// <summary>
    /// Represents Download information for client/server
    /// </summary>
    public class DownloadsGroupInformation
    {
        /// <summary>
        /// Downloads for clients
        /// </summary>
        public DownloadsInformation Client;
        
        /// <summary>
        /// Downloads for server
        /// </summary>
        public DownloadsInformation Server;
    }
}