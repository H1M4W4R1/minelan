using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace MineSharp.Common.Util
{
    public static class Metadata
    {

        /// <summary>
        /// Gets checksum for file
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static string GetSha1(string fileName)
        {
            var str = "";
            using (var fs = new FileStream(fileName, FileMode.Open))
            using (var bs = new BufferedStream(fs))
            {
                using (var sha1 = new SHA1Managed())
                {
                    var hash = sha1.ComputeHash(bs);
                    var formatted = new StringBuilder(2 * hash.Length);
                    
                    foreach (var b in hash)
                    {
                        formatted.AppendFormat("{0:X2}", b);
                    }

                    str = formatted.ToString();
                }
            }

            return str.ToLower();
        }
        
    }
}