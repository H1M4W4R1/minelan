﻿using System;
using System.IO;
using System.Threading;
using MineSharp.Common.Game;
using MineSharp.Common.Network;


namespace MineSharp.DebugClient
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var mc = new LauncherFrame();
            mc.Version = "1.12.2";
            mc.ForgeVersion = "forge-14.23.5.2836";
            mc.Height = 1080;
            mc.Width = 1920;
            mc.Username = "Ishikawa";
            mc.Uuid = "aeef7bc935f9420eb6314dea7ad7e1e5";
            mc.MinRamMb = 1024;
            mc.MaxRamMb = 2048;
            mc.UseForge = true;
            mc.MavenPath = "D:/mvn/bin";
            
            mc.DownloadMinecraft("D:/mcassets", "D:/mcassets/natives", "D:/mcassets/assets", "D:/mcassets/libraries");
            //File.WriteAllText("D:/launch.txt", mc.CreateCommand("D:/mcassets", "D:/mcassets/natives", "D:/mcassets/assets", "D:/mcassets/libraries"));
            mc.RunGame("D:/mcassets", "D:/mcassets/natives", "D:/mcassets/assets", "D:/mcassets/libraries");
            
        }
    }
}