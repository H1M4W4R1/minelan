# MineSharp (Mine#) [WIP]

An Minecraft Launcher Framework written in C#

What does it allow me to do?
----------------------------
* Download Minecraft Version Manifest (versions.json)
* Download Minecraft Version Data File (eg. 1.12.2.json)
* Download Minecraft Version .jar (eg. 1.12.2.jar)
* Download Assets
* Download Libraries
* Run Minecraft
* Forge Support (Run Modded Minecraft)
* Read and iterate through Minecraft Launcher data (json) files

What language it's written in?
------------------------------

It's written entirely in C#

Dependiencies
-------------

* .NET Framework 3.5
* Newtonsoft.Json
* DotNetZip

What game version does it support?
-----------------------------

All versions available in Manifest

Issue rules?
------------

* Don't repeat issue multiple times
* Always remember to paste StackTrace, if not possible then clearly describe your usage of API
